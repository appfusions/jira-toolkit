package com.atlassian.jira.toolkit.customfield;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.MockUser;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.dynamic.P;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestParticipantsCFType extends TestCase
{
    private User alice = new MockUser("alice");
    private Mock mockCommentManager;

    protected void setUp() throws Exception
    {
        super.setUp();
        mockCommentManager = new Mock(CommentManager.class);
        mockCommentManager.setStrict(true);
    }

    public void testGetValueFromIssue() throws Exception
    {
        final JiraAuthenticationContext authenticationContext = createAliceAuthContext();
        // Set up comments with repeated author
        final List<Comment> comments = Arrays.<Comment>asList(
                new MockComment("Dude!", "barney"),
                new MockComment("Awesome", "charlie"),
                new MockComment("Bitchin'", "barney"));
        mockCommentManager.expectAndReturn("getCommentsForUser", P.ANY_ARGS, comments);
        ParticipantsCFType participantsCFType = new ParticipantsCFType(null, authenticationContext, (CommentManager) mockCommentManager.proxy());

        List<User> participants = (List<User>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(4, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockUser("zed")));
        // commenters
        assertTrue(participants.contains(new MockUser("barney")));
        assertTrue(participants.contains(new MockUser("charlie")));
    }

    public void testGetValueFromIssueNullCommentAuthor()
    {
        final JiraAuthenticationContext authenticationContext = createAliceAuthContext();
        // Set up comments with repeated author
        final List<Comment> comments = Arrays.<Comment>asList(
                new MockComment("Dude!", "barney"),
                new MockComment("Awesome", "charlie"),
                new MockComment("Bitchin'", null));
        mockCommentManager.expectAndReturn("getCommentsForUser", P.ANY_ARGS, comments);
        ParticipantsCFType participantsCFType = new ParticipantsCFType(null, authenticationContext, (CommentManager) mockCommentManager.proxy());

        List<User> participants = (List<User>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(4, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockUser("zed")));
        // commenters
        assertTrue(participants.contains(new MockUser("barney")));
        assertTrue(participants.contains(new MockUser("charlie")));
    }

    public void testGetValueFromIssueWithExceptions()
    {
        // Set up comments with exception thrown
        mockCommentManager.expectAndThrow("getCommentsForUser", P.ANY_ARGS, new RuntimeException());
        ParticipantsCFType participantsCFType = new ParticipantsCFType(null, createAliceAuthContext(), (CommentManager) mockCommentManager.proxy());

        List<User> participants = (List<User>) participantsCFType.getValueFromIssue(null, createMockIssue());

        assertEquals(2, participants.size());
        // assignee
        assertTrue(participants.contains(alice));
        // reporter
        assertTrue(participants.contains(new MockUser("zed")));
        // commenters are skipped
    }

    private JiraAuthenticationContext createAliceAuthContext()
    {
        Mock mockAuthenticationContext = new Mock(JiraAuthenticationContext.class);
        mockAuthenticationContext.setStrict(true);

        mockAuthenticationContext.expectAndReturn("getLoggedInUser", alice);
        return (JiraAuthenticationContext) mockAuthenticationContext.proxy();
    }

    private Issue createMockIssue()
    {
        final MockIssue mockIssue = new MockIssue(12);
        mockIssue.setAssignee(alice);
        mockIssue.setReporter(new MockUser("zed"));
        return mockIssue;
    }


    private class MockComment implements Comment
    {
        final String author;
        public MockComment(final String body, final String author)
        {
            this.author = author;
        }

        @Override
        public String getAuthor()
        {
            return author;
        }

        @Override
        public User getAuthorUser()
        {
            return new MockUser(author);
        }

        @Override
        public String getAuthorFullName()
        {
            return null;
        }

        @Override
        public String getBody()
        {
            return null;
        }

        @Override
        public Date getCreated()
        {
            return null;
        }

        @Override
        public String getGroupLevel()
        {
            return null;
        }

        @Override
        public Long getId()
        {
            return null;
        }

        @Override
        public Long getRoleLevelId()
        {
            return null;
        }

        @Override
        public ProjectRole getRoleLevel()
        {
            return null;
        }

        @Override
        public Issue getIssue()
        {
            return null;
        }

        @Override
        public String getUpdateAuthor()
        {
            return null;
        }

        @Override
        public User getUpdateAuthorUser()
        {
            return null;
        }

        @Override
        public String getUpdateAuthorFullName()
        {
            return null;
        }

        @Override
        public Date getUpdated()
        {
            return null;
        }
    }
}
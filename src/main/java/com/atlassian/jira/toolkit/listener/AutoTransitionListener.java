package com.atlassian.jira.toolkit.listener;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * An event listener which triggers a workflow transition under certain conditions. For instance, this can be used to
 * trigger a workflow transition when a user comments or edits an issue in a particular state.
 */
public class AutoTransitionListener extends AbstractIssueEventListener implements IssueEventListener {
    private final static Logger log = Logger.getLogger(AutoTransitionListener.class);

    // Don't change the text for these parameters, as they are used as parameter keys in the database, and the old
    // parameters will still be present and undeletable.
    protected static final String ACTION = "Action ID";
    protected static final String EVENT = "Event ID";
    protected static final String STATUS = "Status";
    protected static final String PROJECT = "Project Key";
    protected static final String CONDITION_REPORTER = "Only do when current user is the Reporter?";
    protected static final String CONDITION_ASSIGNEE = "Only do when current user is the Assignee?";
    protected static final String CONDITION_CUSTOMFIELD = "Only do when current user is in (Multi-) User Picker custom field:";
    protected static final String CONDITION_USER = "Only do when current user is the following?";

    private int actionId;
    private long eventId;
    private int statusId;
    private boolean allProjects = false;
    private String[] projectKeys;

    private boolean currentIsReporter = false;
    private boolean currentIsAssignee = false;
    private String currentIsInCustomField = null;
    private String currentIsUser = null;

    public void init(Map params) {
        if (params.containsKey(ACTION)) {
            actionId = Integer.parseInt((String) params.get(ACTION));
        }
        if (params.containsKey(EVENT)) {
            eventId = Long.parseLong((String) params.get(EVENT));
        }
        if (params.containsKey(STATUS)) {
            statusId = Integer.parseInt((String) params.get(STATUS));
        }
        if (params.containsKey(PROJECT)) {
            String projectList = ((String) params.get(PROJECT)).trim();
            if ("*".equals(projectList)) allProjects = true;
            else projectKeys = projectList.split(",");
        }
        if (params.containsKey(CONDITION_ASSIGNEE)) {
            currentIsAssignee = (Boolean.valueOf((String) params.get(CONDITION_ASSIGNEE))).booleanValue();
        }
        if (params.containsKey(CONDITION_REPORTER)) {
            currentIsReporter = (Boolean.valueOf((String) params.get(CONDITION_REPORTER))).booleanValue();
        }
        if (params.containsKey(CONDITION_CUSTOMFIELD)) {
            currentIsInCustomField = (String) params.get(CONDITION_CUSTOMFIELD);
        }
        if (params.containsKey(CONDITION_USER)) {
            currentIsUser = (String) params.get(CONDITION_USER);
        }

        if (log.isInfoEnabled()) log.info(getInfoMessage());
    }

    private String getInfoMessage()
    {
        String projects = (allProjects ? "all projects" : (projectKeys.length == 0 ? "no projects (ie. disabled)" : (projectKeys.length > 1 ? "projects " : "project ")));
        for (int i=0; i<projectKeys.length; i++)
        {
            projects += projectKeys[i];
            if (i+1 == projects.length()) projects += ", ";
        }

        return "AutoTransitionListener configured to transition issues in "+projects+" in status "+statusId+
                " on event "+eventId+" via action "+actionId+", "+
                (currentIsAssignee ? "if user is assignee; " : "")+
                (currentIsReporter ? "if user is reporter; " : "")+
                (currentIsInCustomField!=null ? "if user is in customfield "+currentIsInCustomField : "")+
                (currentIsUser!=null ? "if user is "+ currentIsUser : "");
    }

    public String[] getAcceptedParams() {
        return new String[]{EVENT, ACTION, STATUS, PROJECT, CONDITION_REPORTER, CONDITION_CUSTOMFIELD, CONDITION_ASSIGNEE, CONDITION_USER};
    }

    public void workflowEvent(IssueEvent event) {

        if (event.getEventTypeId().longValue() == eventId) {
            Issue issue = event.getIssue();
            GenericValue issueGV = event.getIssue().getGenericValue();
            if (isCorrectStatus(issueGV) && isCorrectProject(issueGV)) {
                String username = getCurrentUsername(event);

                if (isCorrectUser(issue, username))
                {
                    log.debug("User "+username+" triggered autotransition action "+actionId+" on issue "+issue);

                    // turn on indexing, in case we were triggered from a workflow post-function
                    // where indexing would be off. Note that nesting workflow transitions is still
                    // not recommended - see http://developer.atlassian.com/jira/browse/JTOOL-28
                    boolean wasIndexing = ImportUtils.isIndexIssues();
                    ImportUtils.setIndexIssues( true );

                    WorkflowTransitionUtil workflowTransitionUtil = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
                    final MutableIssue issueObject = ComponentAccessor.getIssueFactory().getIssue(issueGV);
                    workflowTransitionUtil.setIssue(issueObject);
                    workflowTransitionUtil.setUsername(username);
                    workflowTransitionUtil.setAction(actionId);

                    // JRA-11933 - we want to populate a field values holder with the existing values so we don't
                    // overwrite any values in JIRA (summary and issue type). NOTE: we must do this AFTER we
                    // have set the issue, user and actionId on the transitionUtil since we need these values to
                    // correctly get the FieldScreenRenderer.
                    workflowTransitionUtil.setParams(getPopulatedFieldValuesHolder(workflowTransitionUtil, issueObject));
                    workflowTransitionUtil.validate();
                    workflowTransitionUtil.progress();

                    // set indexing back to previous
                    ImportUtils.setIndexIssues( wasIndexing );
                }
            }
        }
    }

    /**
     * @return Whether the current user qualifies to trigger this autotransition.
     */
    boolean isCorrectUser(Issue issue, String username)
    {
        boolean transition = !currentIsAssignee && !currentIsReporter && currentIsInCustomField == null && currentIsUser == null; // if there's no conditions, transition by default

        // If any conditions are set, any one of them evaluating to true will cause us to transition.
        if (currentIsAssignee && isAssignee(issue, username) ||
        (currentIsReporter && isReporter(issue, username)) ||
        (currentIsInCustomField!=null && isInCustomField(issue, username)) ||
        (currentIsUser!=null && username.equals(currentIsUser)) ) transition = true;
        return transition;
    }

    /**
     * @return Whether we restrict to assignee and the current user is the assignee.
     **/
    private boolean isAssignee(Issue issue, String username)
    {
        return issue.getAssigneeId().equals(username);
    }

    /**
     * @return Whether we restrict to reporter and the current user is the reporter.
     **/
    private boolean isReporter(Issue issue, String username)
    {
        return issue.getReporterId().equals(username);
    }

    private Map getPopulatedFieldValuesHolder(WorkflowTransitionUtil workflowTransitionUtil, Issue issue)
    {
        Map fieldValuesHolder = new HashMap();
        final FieldScreenRenderer fieldScreenRenderer = workflowTransitionUtil.getFieldScreenRenderer();
        for (Iterator iterator = fieldScreenRenderer.getFieldScreenRenderTabs().iterator(); iterator.hasNext();)
        {
            FieldScreenRenderTab fieldScreenRenderTab = (FieldScreenRenderTab) iterator.next();
            for (Iterator iterator1 = fieldScreenRenderTab.getFieldScreenRenderLayoutItems().iterator(); iterator1.hasNext();)
            {
                FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem = (FieldScreenRenderLayoutItem) iterator1.next();
                if (fieldScreenRenderLayoutItem.isShow(issue))
                {
                    fieldScreenRenderLayoutItem.populateFromIssue(fieldValuesHolder, issue);
                }
            }
        }

        return fieldValuesHolder;
    }

    /**
     * @return Whether we restrict to users in a userpicker custom field.
     **/
    private boolean isInCustomField(Issue issue, String username)
    {
        CustomField cf = getRestrictingCustomField(currentIsInCustomField);
        if (cf != null)
        {
            Collection ccUsers = (Collection)issue.getCustomFieldValue(cf);
            if (ccUsers == null) return false;
            Iterator iter = ccUsers.iterator();
            while (iter.hasNext())
            {
                Object o = iter.next();
                if (! (o instanceof User))
                {
                    log.error("AutoTransitionListener configured to check custom field " + cf + " which is returning object " + o +
                            " of type " + o.getClass() + " rather than a com.opensymphony.User. "+
                            "Please reconfigure with A userpicker or multi-userpicker custom field.");
                    return false;
                }
                User u = (User) o;
                if (u.getName().equals(username))
                {
                    log.debug("User "+username+" is in "+cf.getName()+" custom field for issue "+issue+"; allowing autotransition.");
                    return true;
                }
            }
            log.debug("User " + username + " not found in required custom field " + cf.getName() + " for issue " + issue+"; not allowing autotransition");
            return false;
        }
        else return false;
    }

    /**
     * Parse the user's entry (if any) for a Cc user picker custom field.
     * @param customfieldId eg. "customfield_10000"
     * @return The specified custom field.
     */
    private CustomField getRestrictingCustomField(String customfieldId)
    {
        if (customfieldId == null) return null;
        if ("false".equals(customfieldId)) return null;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        CustomField field = customFieldManager.getCustomFieldObject(customfieldId);
        if (field == null) log.error("AutoTransitionListener configured to restrict transitions to custom field with id '" +
                customfieldId + "', but no such custom field was found. Value expected to be customfield_<id>, eg. customfield_10000.");
        return field;
    }

    private boolean isCorrectStatus(GenericValue issueGV) {
        return Integer.parseInt(issueGV.getString("status")) == statusId;
    }

    private boolean isCorrectProject(GenericValue issueGV) {
        if (allProjects) return true;
        String pKey = issueGV.getString("key");
        for (int i = 0; i < projectKeys.length; i++)
        {
            if (pKey.startsWith(projectKeys[i].trim()) || projectKeys[i].equals("*"))
            {
                return true;
            }
        }

        return false;
    }

    private String getCurrentUsername(IssueEvent event) {
        User user = event.getUser();
        String username;
        if (user != null) {
            username = user.getName();
        } else {
            username = event.getComment().getAuthor();
        }
        return username;
    }


    //whether administrators can delete this listener
    public boolean isInternal() {
        return false;
    }

    /**
     * Mail Listeners are unique.  It would be a rare case when you would want two emails sent out.
     */
    public boolean isUnique() {
        return false;
    }

    public String getDescription() {
        return "Transitions an issue given an event to listen for and an action to perform. Fields are as follows:"+
                "<ul style='{ li { margin-left: 10em } }'>"+
                "<li style='margin-top: 1em'><b>"+EVENT+"</b> - ID of a JIRA Event to listen to. Current possibilities are:<br>"+
                "<select multiple='multiple' size='5' disabled='true'>"+getEventsTable()+"</select>"+
                "<li style='margin-top: 1em'><b>"+ACTION+"</b> - Workflow action to trigger (if the conditions pass). Possible values are:<br>"+
                "<select multiple='multiple' size='15' disabled='true'>"+getActions()+"</select>"+
                "<li style='margin-top: 1em'><b>"+STATUS+"</b> - Only trigger for issues in this status. Possible values are:<br>"+
                "<select multiple='multiple' size='15' disabled='true'>"+getStatuses()+"</select>"+
                "<li style='margin-top: 1em'><b>"+PROJECT+"</b> - Only trigger for issues in these projects. This is a comma-separated list of keys. Possible keys are:<br>"+
                "<select multiple='multiple' size='3' disabled='true'>"+getProjectKeys()+"</select>"+
                "<li style='margin-top: 1em'><b>"+CONDITION_REPORTER+"</b> - Whether to trigger only when the current user reported the issue (or when other conditions match)"+
                "<li style='margin-top: 1em'><b>"+CONDITION_CUSTOMFIELD+"</b> - Whether to trigger only when the current user is listed in an issue's user-picker custom field"+
                "(or when other conditions match). The particular custom field to examine is indicated by the ID here. Possible values are:<br>"+
                "<select multiple='multiple' size='15' disabled='true'>"+getCustomFields()+"</select>"+
                "<li style='margin-top: 1em'><b>"+CONDITION_ASSIGNEE+"</b> - Whether to trigger only when the current user is the issue assignee (or when other conditions match)"+
                "</ul>";
    }

    private String getCustomFields() {
        StringBuffer buf = new StringBuffer();
        Collection custFields = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects();
        Iterator iter = custFields.iterator();
        while (iter.hasNext()) {
            CustomField cf = (CustomField) iter.next();
            buf.append("<option><b>").append(cf.getId()).append("</b> - ").append(cf.getName()).append("</option>");
        }
        return buf.toString();
    }

    private String getProjectKeys() {
        StringBuffer buf = new StringBuffer();
        ProjectManager projectManager = ComponentAccessor.getProjectManager();
        Collection projects = projectManager.getProjects();
        Iterator iter = projects.iterator();
        while (iter.hasNext()) {
            GenericValue projectGV = (GenericValue) iter.next();
            Project proj = projectManager.getProjectObj(projectGV.getLong("id"));
            buf.append("<option><b>").append(proj.getKey()).append("</b></option>");
        }
        return buf.toString();

    }

    private String getEventsTable() {
        StringBuffer buf = new StringBuffer();
        Collection eventTypes = ComponentAccessor.getEventTypeManager().getEventTypes();
        Iterator iter = eventTypes.iterator();
        while (iter.hasNext()) {
            EventType e = (EventType) iter.next();
            buf.append("<option><b>").append(e.getId()).append("</b> - ").append(e.getName()).append("</option>");
        }
        return buf.toString();
    }

    private String getActions() {
        StringBuffer buf = new StringBuffer();
        Collection workflows = ComponentAccessor.getWorkflowManager().getWorkflows();
        Iterator wfIter = workflows.iterator();
        while (wfIter.hasNext())
        {
            JiraWorkflow wf = (JiraWorkflow) wfIter.next();
            buf.append("<optgroup label='"+wf.getName()+"'>");
            Iterator actionIter = wf.getAllActions().iterator();
            buf.append("<ul>");
            while (actionIter.hasNext()) {
                ActionDescriptor action = (ActionDescriptor) actionIter.next();
                String srcStep = "•";
                if (action.getParent() instanceof StepDescriptor) {
                    StepDescriptor step = (StepDescriptor) action.getParent();
                    srcStep = step.getName();
                }
                int destStepId = action.getUnconditionalResult().getStep();
                String destStep = wf.getDescriptor().getStep(destStepId).getName();
                buf.append("<option><b>" + action.getId() + "</b> <i>("+action.getName()+")</i>: " + srcStep + " → " + destStep);
            }
            buf.append("</optgroup>");
        }
        return buf.toString();
    }

    /**
     * @return A HTML list of options.
     */
    private String getStatuses() {
        StringBuffer buf = new StringBuffer();
        Collection statuses = ComponentAccessor.getConstantsManager().getStatusObjects();
        Iterator iter = statuses.iterator();
        while (iter.hasNext()) {
            Status status = (Status) iter.next();
            buf.append("<option><b>").append(status.getId()).append("</b> - ").append(status.getName()).append("</option>");
        }
        return buf.toString();
    }

}

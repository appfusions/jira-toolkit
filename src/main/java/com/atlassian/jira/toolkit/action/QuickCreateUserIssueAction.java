package com.atlassian.jira.toolkit.action;

import com.atlassian.core.util.RandomGenerator;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.URLCodec;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.opensymphony.util.TextUtils;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

public class QuickCreateUserIssueAction extends JiraWebActionSupport
{
    private final ConstantsManager constantsManager;
    private final IssueFactory issueFactory;
    private final IssueCreationHelperBean issueCreationHelperBean;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserUtil userUtil;
    private final CrowdService crowdService;
    private final UserPropertyManager userPropertyManager;

    String emailAddress;
    String issueSummary;
    String issueType;
    String assignee;
    String projectId;
    User user;
    String newUserEmailAddress;
    String newUserFullName;
    Collection errorMessages;
    Boolean userExists;
    String phoneNumber;
    private static final String META_PROPERTY_PREFIX = "jira.meta.";
    private static final String PHONE_NUMBER = "phoneNumber";
    Boolean reporteriscurrentuser;

    public QuickCreateUserIssueAction(ConstantsManager constantsManager, IssueFactory issueFactory,
            IssueCreationHelperBean issueCreationHelperBean, JiraAuthenticationContext jiraAuthenticationContext,
            UserUtil userUtil, CrowdService crowdService, UserPropertyManager userPropertyManager)
    {
        this.constantsManager = constantsManager;
        this.issueFactory = issueFactory;
        this.issueCreationHelperBean = issueCreationHelperBean;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userUtil = userUtil;
        this.crowdService = crowdService;
        this.userPropertyManager = userPropertyManager;
    }

    public String doCreateUserIssue()
    {
        //get values to initally populate the create user screen

        if (reporteriscurrentuser.equals(Boolean.FALSE))
        {
            this.user = getUser(emailAddress);
            if (getIsNewUser())
            {
                setNewuserfullname("");
                setPhonenumber("");
            }
            else
            {
                setNewuserfullname(user.getDisplayName());
                setEmailaddress(user.getEmailAddress());
                setPhonenumber(userPropertyManager.getPropertySet(user).getString(META_PROPERTY_PREFIX + PHONE_NUMBER));
            }
        }
        else
        {
            user = jiraAuthenticationContext.getUser();
            return getRedirect(getRedirectToIssueCreateURL());
        }
        return "createuser";
    }

    public boolean getIsNewUser(){
        return this.user == null;
    }

    public String doCreateUser()
    {
        user = getUser(emailAddress);
        userValidation(emailAddress, newUserFullName);
        if (hasAnyErrors())
            return ERROR;

        if (user == null)
        {
            createNewUser();
        }
        else
        {
            UserTemplate updatedUser = new UserTemplate(user);
            updatedUser.setDisplayName(newUserFullName);
            updatedUser.setEmailAddress(emailAddress);

            try
            {
                crowdService.updateUser(updatedUser);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Error updating user: " + user, e);
            }
        }

        //set the phone number to what is in the phone number box
        setUserPhoneNumber(this.phoneNumber);
        return getRedirect(getRedirectToIssueCreateURL());
    }

    private void setUserPhoneNumber(String phoneNumber)
    {
        userPropertyManager.getPropertySet(user).setString(META_PROPERTY_PREFIX + PHONE_NUMBER, phoneNumber);
    }

    private void createNewUser()
    {

        String password = RandomGenerator.randomPassword();
        try
        {
            String username = emailAddress;
            user = userUtil.createUserWithNotification(username, password, emailAddress, newUserFullName, UserEventType.USER_CREATED);
            log.debug("Created user " + emailAddress + ".");
        }
        catch (Exception e)
        {
            log.error("OSUser does not appear to be configured correctly!", e);
        }
    }

    private void userValidation(String addressToValidate, String fullname)
    {
        if (!TextUtils.stringSet(addressToValidate))
        {
            addError("emailaddress", "Email address required");
        }
        else if (!TextUtils.verifyEmail(addressToValidate))
        {
            addError("emailaddress", "Invalid email address");
        }
        if (!TextUtils.stringSet(fullname))
        {
            addError("fullname", "Full name required");
        }

    }

    private String getRedirectToIssueCreateURL()
    {
        return "CreateIssueDetails!init.jspa?pid=" + getProjectId() + "&issuetype=" + issueType + "&summary=" + getURLFriendlySummary() + "&reporter=" + user.getName() + assigneeURL();
    }

    private String assigneeURL()
    {
        if (assignee == null)
        {
            return "&assignee=-1";
        }
        else
        {
            return "&assignee=" + assignee;
        }
    }

    private String getURLFriendlySummary()
    {
        if (issueSummary.length() > 0)
        {
            try
            {
                return (URLCodec.encode(issueSummary));
            }
            catch (UnsupportedEncodingException e)
            {
                log.error("Failed to URL encode summary");
            }
        }
        return "";
    }

    private User getUser(String emailaddress)
    {
        return UserUtils.getUserByEmail(emailaddress);
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectid)
    {
        this.projectId = projectid;
    }

    public String getNewuserfullname()
    {
        return this.newUserFullName;
    }

    public void setNewuserfullname(String newuserfullname)
    {
        this.newUserFullName = newuserfullname;
    }

    public void setEmailaddress(String emailaddress)
    {
        this.emailAddress = emailaddress;
    }

    public String getEmailaddress()
    {
        return emailAddress;
    }

    public void setIssuesummary(String issuesummary)
    {
        this.issueSummary = issuesummary;
    }

    public String getIssuesummary()
    {
        return issueSummary;
    }

    public void setIssuetype(String issuetype)
    {
        this.issueType = issuetype;
    }

    public String getIssuetype()
    {
        return issueType;
    }

    public void setPhonenumber(String phonenumber)
    {
        this.phoneNumber = phonenumber;
    }

    public String getPhonenumber()
    {
        if (phoneNumber != null)
            return this.phoneNumber;
        else
            return "";
    }

    public void setReporteriscurrentuser(Boolean reporteriscurrentuser)
    {
        this.reporteriscurrentuser = reporteriscurrentuser;
    }

    public Boolean getReporteriscurrentuser()
    {
        return reporteriscurrentuser;
    }

    public String getAssignee()
    {
        return assignee;
    }

    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }

}
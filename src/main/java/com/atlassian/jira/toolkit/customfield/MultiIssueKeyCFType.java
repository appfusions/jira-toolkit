package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.toolkit.RequestAttributeKeys;
import com.atlassian.jira.web.action.issue.IssueNavigator;
import com.atlassian.query.order.OrderBy;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MultiIssueKeyCFType extends CalculatedCFType
{

    public String getStringFromSingularObject(Object singularObject)
    {
        return null;
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        return null;
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        return null;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        IssueNavigator nav =   (IssueNavigator) JiraAuthenticationContextImpl.getRequestCache().get(RequestAttributeKeys.MULTIKEY_ACTION);
        List keys = (List)JiraAuthenticationContextImpl.getRequestCache().get(RequestAttributeKeys.MULTIKEY_SEARCHING);
        SearchResults res = null;
        try
        {
            res = nav.getSearchResults();
        }
        catch (Exception e)
        {
            // TODO: Comment why this is safe to ignore
        }

        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);
        if (res != null && issue != null)
        {
            List issues = res.getIssues();
            Issue cur;
            Issue prev = null;
            for (Iterator i = issues.iterator(); i.hasNext();)
            {
                cur = (Issue)i.next();
                if (cur.getKey().equals(issue.getKey()))
                {
                    if (prev != null)
                    {
                        params.put("previousIssue", prev.getKey());
                    }
                    if (i.hasNext())
                    {
                        params.put("nextIssue", ((Issue)i.next()).getKey());
                    }
                }
                prev = cur;
            }
            if(res.getEnd() == res.getTotal())
            {
                params.put("lastPage", "lastPage");
            }
            if (res.getStart() == 0)
            {
                params.put("firstPage", "firstPage");
            }
        }
        // This has nothing to do we ME (dylan). I am just hacking a hack of a hack, blame nmenere
        if (nav != null && nav.getSearchRequest() != null && nav.getSearchRequest().getQuery() != null)
        {
            final OrderBy orderBy = nav.getSearchRequest().getQuery().getOrderByClause();
            if (orderBy != null && !orderBy.getSearchSorts().isEmpty())
            {
                params.put("sorter", orderBy.getSearchSorts().get(0));
            }
        }
        if (keys != null && keys.size() != 0)
        {
            params.put("isBeingUsed", "isBeingUsed");
        }
        return params;
    }
}


package com.atlassian.jira.toolkit.systemfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;

public class PriorityCustomField extends CalculatedCFType
{
    public String getStringFromSingularObject(Object singularObject)
    {
        return (String) singularObject;
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        return string;
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        return issue != null ? issue.getPriority().getString("name") : null;
    }
}

package com.atlassian.jira.toolkit.workflow;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Returns true if the Custom Field is less than/ greater than / or equal to a given value
 */
public class CompareNumberCFCondition extends AbstractJiraCondition
{
    public static final String EQUALS = "equal";
    public static final String LESS_THAN = "less than";
    public static final String GREATER_THAN = "greater than";

    private static final Logger log = Logger.getLogger(CompareNumberCFCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        CustomFieldManager fieldManager = ComponentAccessor.getCustomFieldManager();
        Issue issue = getIssue(transientVars);

        String cfKey = (String) args.get("numbercf");
        String comparator = (String) args.get("comparator");
        String value = (String) args.get("value");

        if (StringUtils.isEmpty(cfKey) || StringUtils.isEmpty(comparator) || StringUtils.isEmpty(value))
        {
            log.warn("Workflow condition " + getClass() + " is not configured correctly");
            return false;
        }
        CustomField field = fieldManager.getCustomFieldObject(cfKey);
        if (field == null)
        {
            log.error("No custom field with key '" + cfKey + "'");
            return false;
        }

        Number cfVal = (Number)issue.getCustomFieldValue(field);

        if (cfVal != null)
        {
            Long longVal = new Long(value);
            if (comparator.equals(EQUALS))
            {
                return cfVal.longValue() ==longVal.longValue();
            }
            else if (comparator.equals(LESS_THAN))
            {
                return cfVal.longValue() < longVal.longValue();
            }
            else if (comparator.equals(GREATER_THAN))
            {
                return cfVal.longValue() > longVal.longValue();
            }
        }

        return false;

    }
}
